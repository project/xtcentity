<?php

namespace Drupal\xtcprofile\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xtc\XtendedContent\API\ToolBox;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtcprofile\Entity\XtcProfile;
use Drupal\xtcserver\Entity\XtcServer;

/**
 * Class XtcProfileForm.
 *
 * @package Drupal\xtcprofile\Form
 */
class XtcProfileForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $xtcprofile = $this->entity;
    if (!empty($this->getSetting('handler'))) {
      $plugin = XtcLoaderHandler::load($this->getSetting('handler'));
    }
    $provider = $plugin['provider'] ?? NULL;

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#description' => $this->t('A human-readable title for this option set.'),
      '#maxlength' => 255,
      '#default_value' => $xtcprofile->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $xtcprofile->id(),
      '#machine_name' => [
        'exists' => '\Drupal\xtcprofile\Entity\XtcProfile::load',
      ],
      '#disabled' => !$xtcprofile->isNew(),
    ];

    // Options Vertical Tab Group table.
    $form['tabs'] = [
      '#type' => 'horizontal_tabs',
    ];

    $form['definition'] = [
      '#type' => 'details',
      '#title' => $this->t('Definition'),
      '#group' => 'tabs',
      '#open' => FALSE,
    ];
    $form['definition'] ['handler'] = [
      '#type' => 'select',
      '#title' => $this->t('Xtended Content Handler'),
      '#description' => $this->t('Select the XTC Handler to access your data.'),
      '#options' => ToolBox::getPlugins('plugin.manager.xtc_handler'),
      '#default_value' => $this->getSetting('handler'),
    ];
    $form['definition'] ['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#description' => $this->t('A description for this option set.'),
      '#default_value' => $this->getSetting('description'),
    ];

    // Add the cardinality sub-form.
    $form['definition'] ['cardinality_container'] = $this->getCardinalityForm();


    if (in_array($provider, ['xtcfile'])) {
      $form['file_options'] = [
        '#type' => 'details',
        '#title' => $this->t('File options'),
        '#group' => 'tabs',
        '#open' => FALSE,
      ];

      $form['file_options']['path_root'] = [
        '#type' => 'select',
        '#title' => $this->t('Path root'),
        '#description' => $this->t('Must be an <em>active</em> module, or a special path.'),
        '#options' => $this->getFileOptions(),
        '#default_value' => $this->getSetting('path_root'),
      ];
      $form['file_options']['path'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Path'),
        '#description' => $this->t('Absolute path from the server root.<br />Relative path from the <em>module</em> directory.'),
        '#default_value' => $this->getSetting('path'),
      ];
      $form['file_options']['filetype'] = [
        '#type' => 'textfield',
        '#title' => $this->t('File type'),
        '#description' => $this->t('File type might be needed for specific handlers.'),
        '#default_value' => $this->getSetting('filetype'),
      ];
    }

    if (in_array($provider, ['xtcgraphql', 'xtcguzzle', 'xtcnotion'])) {
      $form['server_options'] = [
        '#type' => 'details',
        '#title' => $this->t('GraphQL & Guzzle options'),
        '#group' => 'tabs',
        '#open' => FALSE,
      ];
      $form['server_options']['server'] = [
        '#type' => 'select',
        '#title' => $this->t('Server'),
        '#description' => $this->t('Select the XTC Server to access your data.'),
        '#options' => array_merge(['' => '-----'], $this->getXtcServers(), ToolBox::getPlugins('plugin.manager.xtc_server')),
        '#default_value' => $this->getSetting('server'),
      ];
    }

    if (in_array($provider, ['xtcgraphql'])) {
      $form['graphql_options'] = [
        '#type' => 'details',
        '#title' => $this->t('GraphQL options'),
        '#group' => 'tabs',
        '#open' => FALSE,
      ];
      $form['graphql_options']['query'] = [
        '#title' => $this->t('Query'),
        '#type' => 'textarea',
        '#description' => $this->t('A query to send to the GraphQL Server.'),
        '#default_value' => $this->getSetting('query'),
      ];
    }

    if (in_array($provider, ['xtcdrupal'])) {
      $form['drupal_options'] = [
        '#type' => 'details',
        '#title' => $this->t('Drupal options'),
        '#group' => 'tabs',
        '#open' => FALSE,
      ];
      $form['drupal_options']['entity_type_id'] = [
        '#title' => $this->t('Entity Type'),
        '#type' => 'select',
        '#description' => $this->t('Allowed Entity type contents.'),
        '#options' => array_merge(['' => '-----'], ToolBox::getEntityTypes()),
        '#default_value' => $this->getSetting('entity_type_id'),
      ];
      $form['drupal_options']['content_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Identifier'),
        '#description' => $this->t('Content identifier (ex: <b>123</b> for content node/123).'),
        '#default_value' => $this->getSetting('content_id'),
      ];
    }

    if (in_array($provider, ['xtcguzzle', 'xtcnotion'])) {
      $form['guzzle_options'] = [
        '#type' => 'details',
        '#title' => $this->t('Guzzle options'),
        '#group' => 'tabs',
        '#open' => FALSE,
      ];
      $form['guzzle_options']['method'] = [
        '#title' => $this->t('Method'),
        '#type' => 'textfield',
        '#description' => $this->t('A method to send to the GraphQL Server.'),
        '#default_value' => $this->getSetting('method'),
      ];
      $form['guzzle_options']['token'] = [
        '#type' => 'details',
        '#title' => $this->t('Token'),
        '#group' => 'fieldgroup',
      ];
      $form['guzzle_options']['token']['token_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#description' => $this->t('Select the token type.'),
        '#options' => ['' => '---', 'bearer' => 'Bearer'],
        '#default_value' => $this->getSetting('token_type'),
      ];
      $form['guzzle_options']['token']['token_value'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Value'),
        '#description' => $this->t('Copy your Secret Token.'),
        '#default_value' => $this->getSetting('token_value'),
      ];
    }

    return $form;
  }

  /**
   * Builds the cardinality form.
   *
   * @return array
   *   The cardinality form render array.
   */
  protected function getCardinalityForm() {
    $form = [
      // Reset #parents so the additional container does not appear.
      '#parents' => [],
      '#type' => 'fieldset',
      '#title' => $this->t('Allowed number of values'),
      '#attributes' => [
        'class' => [
          'container-inline',
          'fieldgroup',
          'form-composite',
        ],
      ],
    ];

    $form['#element_validate'][] = '::validateCardinality';
    $cardinality = $this->getSetting('cardinality');
    $form['cardinality_limit'] = [
      '#type' => 'select',
      '#title' => $this->t('Allowed number of values'),
      '#title_display' => 'invisible',
      '#options' => [
        'number' => $this->t('Limited'),
        FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED => $this->t('Unlimited'),
      ],
      '#default_value' => ($cardinality == FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) ? FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED : 'number',
    ];
    $form['cardinality_number'] = [
      '#type' => 'number',
      '#default_value' => $cardinality != FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED ? $cardinality : 1,
      '#min' => 1,
      '#title' => $this->t('Limit'),
      '#title_display' => 'invisible',
      '#size' => 2,
      '#states' => [
        'visible' => [
          ':input[name="cardinality_limit"]' => ['value' => 'number'],
        ],
        'disabled' => [
          ':input[name="cardinality_limit"]' => ['value' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Validates the cardinality.
   *
   * @param array $element
   *   The cardinality form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateCardinality(array &$element, FormStateInterface $form_state) {
    // Validate field cardinality.
    if ($form_state->getValue('cardinality') === 'number' && !$form_state->getValue('cardinality_number')) {
      $form_state->setError($element['cardinality_number'], $this->t('Number of values is required.'));
    }
    $this->setCardinality($form_state);
  }

  protected function setCardinality(FormStateInterface $form_state) {
    if ($form_state->getValue('cardinality_limit') == FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      $form_state->setValue('cardinality', FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    }
    else {
      $form_state->setValue('cardinality', $form_state->getValue('cardinality_number'));
    }
  }

  /**
   * @return array|\string[][]
   */
  protected function getFileOptions() {
    $special_path = [
      'abs_path' => 'Absolute path',
      'config-sync' => 'Configuration Sync directory',
      'public' => 'Public files path',
      //      'private' => 'Private files path',
    ];
    return array_merge(['Special path' => $special_path], ToolBox::getFlySystems(), ToolBox::getModulesList());
  }

  /**
   * @return array
   */
  protected function getXtcServers() {
    $xtcServers = [];
    $servers = XtcServer::loadMultiple();
    foreach ($servers as $id => $server) {
      $options = $server->get('options');
      $xtcServers['XTC Server › ' . $options['type']][$id] = $server->label()
        . ' (' . $server->id() . ')';
    }
    return $xtcServers;
  }

  protected function getSetting($name) {
    $options = $this->entity->getOptions();
    return isset($options[$name]) ? $options[$name] : XtcProfile::getDefault($name);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\xtcprofile\Entity\XtcProfile $entity */
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addStatus($this->t('Created the %label XTC Profile optionset.', [
            '%label' => $entity->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addStatus($this->t('Saved the %label XTC Profile optionset.', [
            '%label' => $entity->label(),
          ]));
    }
    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $options = [];
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      if (in_array($key, ['id', 'label'])) {
        $entity->set($key, $value);
      }
      else {
        $options[$key] = $value;
      }
    }
    $entity->set('options', $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    // Prevent access to delete button when editing default configuration.
    if ($this->entity->id() == 'default' && isset($actions['delete'])) {
      $actions['delete']['#access'] = FALSE;
    }
    return $actions;
  }

  /**
   * Validation functions.
   */
  public function validateNamespace(array &$element, FormStateInterface $form_state) {
    // @todo
    // @see form_error()
    return TRUE;
  }

  /**
   * Validation functions.
   */
  public function validateSelector(array &$element, FormStateInterface $form_state) {
    // @todo
    // @see form_error()
    return TRUE;
  }

  /**
   * Validate thumbnail option values.
   *
   * Empties the value of the thumbnail caption option when the paging control
   * is not set to thumbnails.
   *
   * @param array $element
   *   The element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateThumbnailOptions(array &$element, FormStateInterface $form_state) {
    if ($form_state->getValue('controlNav') !== 'thumbnails' && $element['#value']) {
      $form_state->setValueForElement($element, '');
    }
  }

}
