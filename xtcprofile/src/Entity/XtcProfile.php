<?php

namespace Drupal\xtcprofile\Entity;

use Drupal\xtcentity\Entity\XtcConfigEntityBase;
use Drupal\xtcentity\XtcConfigEntityInterface;
use Drupal\xtcprofile\XtcProfileInterface;

/**
 * Defines the XTC Profile entity.
 *
 * @ConfigEntityType(
 *   id = "xtcprofile",
 *   label = @Translation("XTC Profile"),
 *   handlers = {
 *     "list_builder" = "Drupal\xtcentity\Controller\XtcEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\xtcprofile\Form\XtcProfileForm",
 *       "edit" = "Drupal\xtcprofile\Form\XtcProfileForm",
 *       "delete" = "Drupal\xtcentity\Form\XtcEntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "optionset",
 *   admin_permission = "administer xtcprofile",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/xtc/xtcprofile/{xtcprofile}",
 *     "edit-form" = "/admin/config/xtc/xtcprofile/{xtcprofile}/edit",
 *     "enable" = "/admin/config/xtc/xtcprofile/{xtcprofile}/enable",
 *     "disable" = "/admin/config/xtc/xtcprofile/{xtcprofile}/disable",
 *     "delete-form" = "/admin/config/xtc/xtcprofile/{xtcprofile}/delete",
 *     "collection" = "/admin/config/xtc/xtcprofile"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "options",
 *     "cardinality"
 *   }
 * )
 */
class XtcProfile extends XtcConfigEntityBase implements XtcConfigEntityInterface {

  const DEFAULT = [
    'handler' => '',
    'description' => '',
    'path_root' => '',
    'path' => '',
    'filetype' => '',
    'server' => '',
    'query' => '',
    'method' => '',
  ];

}
