<?php

/**
 * @file
 * Contains xtended_content.page.inc.
 *
 * Page callback for Xtended Content entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Xtended Content templates.
 *
 * Default template: xtended_content.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_xtended_content(array &$variables) {
  // Fetch XtendedContent Entity Object.
  $xtended_content = $variables['elements']['#xtended_content'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
