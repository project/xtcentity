<?php

namespace Drupal\xtcentity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Xtended Content entities.
 *
 * @ingroup xtcentity
 */
class XtendedContentDeleteForm extends ContentEntityDeleteForm {


}
