<?php

namespace Drupal\xtcentity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xtc\XtendedContent\API\ToolBox;
use Drupal\xtcentity\Entity\XtendedContentType;

/**
 * Class XtendedContentTypeForm.
 */
class XtendedContentTypeForm extends EntityForm
{

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state)
  {
    $form = parent::form($form, $form_state);

    $xtended_content_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $xtended_content_type->label(),
      '#description' => $this->t("Label for the Xtended Content type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $xtended_content_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\xtcentity\Entity\XtendedContentType::load',
      ],
      '#disabled' => !$xtended_content_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */
    $form['additional_settings'] = [
      '#type' => 'horizontal_tabs',
      '#attached' => [
        'library' => ['node/drupal.content_types'],
      ],
    ];

    $form['xtc_handlers'] = [
      '#type' => 'details',
      '#title' => t('Xtended Content Handlers'),
      '#group' => 'additional_settings',
    ];
    $form['xtc_handlers']['xtchandlers'] = [
      '#type' => 'select',
      '#title' => t('Handlers'),
      '#options' => $this->getHandlers(),
      '#default_value' => $xtended_content_type->getXtcHandlers(),
      '#description' => t('Select XTC Handler types to be enabled.'),
    ];

    $form['xtc_verbs'] = [
      '#type' => 'details',
      '#title' => t('Xtended Content Verbs'),
      '#group' => 'additional_settings',
    ];
    $form['xtc_verbs']['xtcverbs'] = [
      '#type' => 'checkboxes',
      '#title' => t('Verbs'),
      '#options' => ToolBox::getVerbs(),
      '#default_value' => $xtended_content_type->getXtcVerbs(),
      '#description' => t('Select XTC Verbs to be allowed.'),
    ];

    return $form;
  }

  protected function getHandlers()
  {
    $handlers = [];
    $plugin_definitions = ToolBox::getPlugins('plugin.manager.xtc_handler');
    foreach ($plugin_definitions as $name => $plugins) {
      if (!empty($name)) {
        $handlers[$name] = $name;
      }
    }
    return $handlers;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $xtended_content_type = $this->entity;
    $form_state->setValue('xtchandlers', $xtended_content_type->getXtcHandlers());

    $status = $xtended_content_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Xtended Content type.', [
          '%label' => $xtended_content_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Xtended Content type.', [
          '%label' => $xtended_content_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($xtended_content_type->toUrl('collection'));
  }

}
