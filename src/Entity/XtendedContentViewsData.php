<?php

namespace Drupal\xtcentity\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Xtended Content entities.
 */
class XtendedContentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
