<?php


namespace Drupal\xtcentity\Entity\Annotation;

use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a content entity type annotation object.
 *
 * Content Entity type plugins use an object-based annotation method, rather than an
 * array-type annotation method (as commonly used on other annotation types).
 * The annotation properties of content entity types are found on
 * \Drupal\xtcentity\Entity\XtcEntity and are accessed using
 * get/set methods defined in \Drupal\xtcentity\Entity\XtcEntityInterface.
 *
 * @ingroup entity_api
 *
 * @Annotation
 */
class XtcEntity extends ContentEntityType
{

  /**
   * {@inheritdoc}
   */
  public $entity_type_class = 'Drupal\xtcentity\Entity\XtcEntity';

  /**
   * {@inheritdoc}
   */
  public $group = 'xtcentity';

  /**
   * {@inheritdoc}
   */
  public function get() {
    $this->definition['group_label'] = new TranslatableMarkup('XTC Entity', [], ['context' => 'XTC Entity type group']);

    return parent::get();
  }

}
