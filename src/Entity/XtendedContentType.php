<?php

namespace Drupal\xtcentity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Xtended Content type entity.
 *
 * @ingroup xtcentity
 *
 * @ConfigEntityType(
 *   id = "xtended_content_type",
 *   label = @Translation("Xtended Content type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\xtcentity\XtendedContentTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\xtcentity\Form\XtendedContentTypeForm",
 *       "edit" = "Drupal\xtcentity\Form\XtendedContentTypeForm",
 *       "delete" = "Drupal\xtcentity\Form\XtendedContentTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\xtcentity\XtendedContentTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "xtended_content_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "xtended_content",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "xtchandlers" = "xtchandlers",
 *     "xtcverbs" = "xtcverbs"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "xtchandlers",
 *     "xtcverbs"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/xtended_content_type/{xtended_content_type}",
 *     "add-form" = "/admin/structure/xtended_content_type/add",
 *     "edit-form" = "/admin/structure/xtended_content_type/{xtended_content_type}",
 *     "delete-form" = "/admin/structure/xtended_content_type/{xtended_content_type}/delete",
 *     "collection" = "/admin/structure/xtended_content_type"
 *   }
 * )
 */
class XtendedContentType extends ConfigEntityBundleBase implements XtendedContentTypeInterface {

  /**
   * The Xtended Content type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Xtended Content type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Xtended Content type handlers.
   *
   * @var array
   */
  protected $xtchandlers;

  /**
   * The Xtended Content type verbs.
   *
   * @var array
   */
  protected $xtcverbs;

  /**
   * {@inheritdoc}
   */
  public function getXtcHandlers()
  {
    return $this->xtchandlers;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtcVerbs()
  {
    return $this->xtcverbs;
  }

  /**
   * {@inheritdoc}
   */
  public function setXtcHandlers($xtchandlers)
  {
    $this->xtchandlers = $xtchandlers;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setXtcVerbs($xtcverbs)
  {
    $this->xtcverbs = $xtcverbs;
    return $this;
  }


}
