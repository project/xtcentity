<?php

namespace Drupal\xtcentity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\xtcentity\XtcConfigEntityInterface;

/**
 * Defines a XTC Config entity.
 *
 */
abstract class XtcConfigEntityBase extends ConfigEntityBase implements XtcConfigEntityInterface {

  const DEFAULT = [
    'tls_dev' => false,
    'host_dev' => 'www.example.com',
    'port_dev' => 80,
    'endpoint_dev' => '',
  ];

  /**
   * The XTC Config entity optionset ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The XTC Config entity optionset label.
   *
   * @var string
   */
  protected $label;

  /**
   * The XTC Config entity optionset options.
   *
   * @var array
   */
  protected $options = [];

  /**
   * {@inheritdoc}
   */
  public function getOptions($strict = FALSE) {
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions(array $options) {
    $this->options = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($name) {
    return isset($this->options[$name]) ? $this->options[$name] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $values = []) {
    $entity = parent::create($values);
    // Merge options with default options.
    $default_options = self::getDefaults();
    $entity->setOptions($entity->getOptions() + $default_options);
    return $entity;
  }

  /**
   * Return defaults values;
   *
   * @return array
   */
  public static function getDefaults():array {
    return static::DEFAULT;
  }

  /**
   * @inheritDoc
   */
  public static function getDefault($name) {
    return static::DEFAULT[$name] ?? null;
  }
}
