<?php


namespace Drupal\xtcentity\Entity;


interface XtcEntityInterface
{

  /**
   * Gets the Xtended Content xtchandlers.
   *
   * @return array
   *   XTC Handlers of the Xtended Content.
   */
  public function getXtcHandlers();

  /**
   * Sets the Xtended Content xtchandlers.
   *
   * @param array $xtchandlers
   *   The Xtended Content XTC Handlers.
   *
   * @return \Drupal\xtcentity\Entity\XtendedContentInterface
   *   The called Xtended Content entity.
   */
  public function setXtcHandlers($xtchandlers);


}
