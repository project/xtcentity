<?php

namespace Drupal\xtcentity\Entity;

use Composer\Autoload\ClassLoader;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Xtended Content entity.
 *
 * @ingroup xtcentity
 *
 * @ContentEntityType(
 *   id = "xtended_content",
 *   label = @Translation("Xtended Content"),
 *   bundle_label = @Translation("Xtended Content type"),
 *   handlers = {
 *     "view_builder" = "Drupal\xtcentity\Entity\XtendedContentViewBuilder",
 *     "list_builder" = "Drupal\xtcentity\XtendedContentListBuilder",
 *     "views_data" = "Drupal\xtcentity\Entity\XtendedContentViewsData",
 *     "translation" = "Drupal\xtcentity\XtendedContentTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\xtcentity\Form\XtendedContentForm",
 *       "add" = "Drupal\xtcentity\Form\XtendedContentForm",
 *       "edit" = "Drupal\xtcentity\Form\XtendedContentForm",
 *       "delete" = "Drupal\xtcentity\Form\XtendedContentDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\xtcentity\XtendedContentHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\xtcentity\XtendedContentAccessControlHandler",
 *   },
 *   base_table = "xtended_content",
 *   data_table = "xtended_content_field_data",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer xtended content entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/xtc/{xtended_content}",
 *     "add-page" = "/xtc/add",
 *     "add-form" = "/xtc/add/{xtended_content_type}",
 *     "edit-form" = "/xtc/{xtended_content}/edit",
 *     "delete-form" = "/xtc/{xtended_content}/delete",
 *     "collection" = "/admin/content/xtended_content",
 *   },
 *   bundle_entity_type = "xtended_content_type",
 *   field_ui_base_route = "entity.xtended_content_type.edit_form"
 * )
 */
class XtendedContent extends ContentEntityBase implements XtendedContentInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * @var XtendedContentType
   */
  var $xtctype;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $entity = self::loadXtcEntity($values['type']);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
      'xtctype' => $entity,
    ];
  }

  public static function loadXtcEntity($bundle) {
    return \Drupal::entityTypeManager()
      ->getStorage('xtended_content_type')
      ->load($bundle);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  public function getXtcHandlers() {
    if(empty($this->xtctype)){
      $this->xtctype = self::loadXtcEntity($this->bundle());
    }
    return $this->xtctype->getXtcHandlers();
  }

  public function getXtcVerbs() {
    if(empty($this->xtctype)){
      $this->xtctype = self::loadXtcEntity($this->bundle());
    }
    return $this->xtctype->getXtcVerbs();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Xtended Content entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Xtended Content entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['xtcprofiles'] = BaseFieldDefinition::create('xtcfield_plugin_profile')
      ->setLabel(t('XTC Profile'))
      ->setDescription(t('Select XTC Profile to be enabled.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'select',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    $fields['status']->setDescription(t('A boolean indicating whether the Xtended Content is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }
}
