<?php


namespace Drupal\xtcentity\Entity;


use Drupal\Core\Entity\ContentEntityType;

class XtcEntity extends ContentEntityType implements XtcEntityInterface
{

  /**
   * The Xtended Content type handlers.
   *
   * @var array
   */
  protected $xtchandlers;

  /**
   * {@inheritdoc}
   */
  public function getXtcHandlers()
  {
    return $this->xtchandlers;
  }

  /**
   * {@inheritdoc}
   */
  public function setXtcHandlers($xtchandlers)
  {
    $this->xtchandlers = $xtchandlers;
    return $this;
  }

}
