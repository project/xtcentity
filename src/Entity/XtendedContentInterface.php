<?php

namespace Drupal\xtcentity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Xtended Content entities.
 *
 * @ingroup xtcentity
 */
interface XtendedContentInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Xtended Content name.
   *
   * @return string
   *   Name of the Xtended Content.
   */
  public function getName();

  /**
   * Sets the Xtended Content name.
   *
   * @param string $name
   *   The Xtended Content name.
   *
   * @return \Drupal\xtcentity\Entity\XtendedContentInterface
   *   The called Xtended Content entity.
   */
  public function setName($name);

  /**
   * Gets the Xtended Content creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Xtended Content.
   */
  public function getCreatedTime();

  /**
   * Sets the Xtended Content creation timestamp.
   *
   * @param int $timestamp
   *   The Xtended Content creation timestamp.
   *
   * @return \Drupal\xtcentity\Entity\XtendedContentInterface
   *   The called Xtended Content entity.
   */
  public function setCreatedTime($timestamp);

}
