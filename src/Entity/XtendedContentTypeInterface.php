<?php

namespace Drupal\xtcentity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Xtended Content type entities.
 */
interface XtendedContentTypeInterface {

  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Xtended Content xtchandlers.
   *
   * @return array
   *   XTC Handlers of the Xtended Content.
   */
  public function getXtcHandlers();

  /**
   * Sets the Xtended Content xtchandlers.
   *
   * @param array $xtchandlers
   *   The Xtended Content XTC Handlers.
   *
   * @return \Drupal\xtcentity\Entity\XtendedContentInterface
   *   The called Xtended Content entity.
   */
  public function setXtcHandlers($xtchandlers);

  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Xtended Content xtcverbs.
   *
   * @return array
   *   XTC Verbs of the Xtended Content.
   */
  public function getXtcVerbs();

  /**
   * Sets the Xtended Content xtchandlers.
   *
   * @param array $xtcverbs
   *   The Xtended Content XTC Verbs.
   *
   * @return \Drupal\xtcentity\Entity\XtendedContentInterface
   *   The called Xtended Content entity.
   */
  public function setXtcVerbs($xtcverbs);


}
