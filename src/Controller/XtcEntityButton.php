<?php

namespace Drupal\xtcentity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\xtc\XtendedContent\API\XtcLoaderTrigger;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class XtcEntityButton.
 *
 *  Returns responses for XTC Entity Button routes.
 */
class XtcEntityButton extends ControllerBase {

  /**
   * @param $trigger
   * @param $content_entity_type
   * @param $content
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function trigger($trigger, $content_entity_type, $content) {
    $triggers = XtcLoaderTrigger::get($trigger);
    $results = $triggers->buildContent($content_entity_type, $content);

    $url = Url::fromRoute('entity.node.canonical', ['node' => $content])
      ->toString();
    return new RedirectResponse($url);
  }

}
