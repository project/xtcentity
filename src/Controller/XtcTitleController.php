<?php


namespace Drupal\xtcentity\Controller;

use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

class XtcTitleController extends EntityController {

  /**
   * @inheritDoc
   */
  public function editTitle(RouteMatchInterface $route_match, EntityInterface $_entity = NULL) {
    if ($entity = $this->doGetEntity($route_match, $_entity)) {
      return $this->t('Edit '. $entity->getEntityType()->getLabel()->__toString() . ' %label', ['%label' => $entity->label()]);
    }
  }


}
