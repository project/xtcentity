<?php

namespace Drupal\xtcentity\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\xtcentity\Entity\XtcEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class XtcEntityController.
 *
 *  Returns responses for XTC Entity routes.
 */
class XtcEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a XTC Entity revision.
   *
   * @param int $xtcentity_revision
   *   The XTC Entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($xtcentity_revision) {
    $xtcentity = $this->entityTypeManager()->getStorage('xtcentity')
      ->loadRevision($xtcentity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('xtcentity');

    return $view_builder->view($xtcentity);
  }

  /**
   * Page title callback for a XTC Entity revision.
   *
   * @param int $xtcentity_revision
   *   The XTC Entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($xtcentity_revision) {
    $xtcentity = $this->entityTypeManager()->getStorage('xtcentity')
      ->loadRevision($xtcentity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $xtcentity->label(),
      '%date' => $this->dateFormatter->format($xtcentity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a XTC Entity.
   *
   * @param \Drupal\xtcentity\Entity\XtcEntityInterface $xtcentity
   *   A XTC Entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(XtcEntityInterface $xtcentity) {
    $account = $this->currentUser();
    $xtcentity_storage = $this->entityTypeManager()->getStorage('xtcentity');

    $langcode = $xtcentity->language()->getId();
    $langname = $xtcentity->language()->getName();
    $languages = $xtcentity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $xtcentity->label()]) : $this->t('Revisions for %title', ['%title' => $xtcentity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all xtc entity revisions") || $account->hasPermission('administer xtc entity entities')));
    $delete_permission = (($account->hasPermission("delete all xtc entity revisions") || $account->hasPermission('administer xtc entity entities')));

    $rows = [];

    $vids = $xtcentity_storage->revisionIds($xtcentity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\xtcentity\XtcEntityInterface $revision */
      $revision = $xtcentity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $xtcentity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.xtcentity.revision', [
            'xtcentity' => $xtcentity->id(),
            'xtcentity_revision' => $vid,
          ]));
        }
        else {
          $link = $xtcentity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.xtcentity.translation_revert', [
                'xtcentity' => $xtcentity->id(),
                'xtcentity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.xtcentity.revision_revert', [
                'xtcentity' => $xtcentity->id(),
                'xtcentity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.xtcentity.revision_delete', [
                'xtcentity' => $xtcentity->id(),
                'xtcentity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['xtcentity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
