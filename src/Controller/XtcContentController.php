<?php


namespace Drupal\xtcentity\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\Controller\EntityController;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;

class XtcContentController extends EntityController {

  /**
   * @inheritDoc
   */
  public function content($profile, $id) {
    $value = $this->getContent($profile, $id);
    $content['body'] = $value['content']['rendered'];
    return [
      '#theme' => 'xtc_file',
      '#response' => $content,
    ];
  }

  /**
   * @inheritDoc
   */
  public function getTitle($profile, $id) {
    $value = $this->getContent($profile, $id);
    $title = $value['title']['rendered'];
    return $title;
  }

  protected function getContent($profile, $id) {
    return Json::decode(XtcLoaderProfile::content($profile, ['query' => $id]));
  }

}
