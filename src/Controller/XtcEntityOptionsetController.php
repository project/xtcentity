<?php

namespace Drupal\xtcentity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\xtcentity\Entity\XtcConfigEntityBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Route controller class for the XTC Profile options configuration.
 */
class XtcEntityOptionsetController extends ControllerBase {

  /**
   * Enables a XTC Profile object.
   *
   * @param \Drupal\xtcentity\Entity\XtcConfigEntityBase $xtcentity
   *   The XTC Profile object to enable.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the XTC Profile optionset listing page.
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function enable(XtcConfigEntityBase $xtcentity): RedirectResponse {
    $xtcentity->enable()->save();
    return new RedirectResponse($xtcentity->toUrl('collection', ['absolute' => TRUE])->toString());
  }

  /**
   * Disables an XTC Profile object.
   *
   * @param \Drupal\xtcentity\Entity\XtcConfigEntityBase $xtcentity
   *   The XTC Profile object to disable.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the XTC Profile optionset listing page.
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function disable(XtcConfigEntityBase $xtcentity): RedirectResponse {
    $xtcentity->disable()->save();
    return new RedirectResponse($xtcentity->toUrl('collection', ['absolute' => TRUE])->toString());
  }

}
