<?php

namespace Drupal\xtcentity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for xtended_content.
 */
class XtendedContentTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
