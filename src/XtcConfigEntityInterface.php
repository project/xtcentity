<?php

namespace Drupal\xtcentity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining XTC Configuration entity optionset entities.
 */
interface XtcConfigEntityInterface extends ConfigEntityInterface {

  /**
   * Returns the array of xtc_config_entity options.
   *
   * @param bool $strict
   *   Use strict typecasting, as defined by the xtc_config_entity.
   *   This fixes the typecasting of options that we defined
   *   differently in the schema.
   *
   * @return array
   *   The array of options.
   */
  public function getOptions($strict = FALSE);

  /**
   * Returns the value of a xtc_config_entity option.
   *
   * @param string $name
   *   The option name.
   *
   * @return mixed
   *   The option value.
   */
  public function getOption($name);

  /**
   * Sets the xtc_config_entity options array.
   *
   * @param array $options
   *   New/updated array of options.
   */
  public function setOptions(array $options);

}
