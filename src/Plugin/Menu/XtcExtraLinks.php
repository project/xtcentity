<?php

namespace Drupal\xtcentity\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

/**
 * Represents a menu link for a single Product.
 */
class XtcExtraLinks extends MenuLinkDefault {}
