<?php

namespace Drupal\xtcentity\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Theme\Registry;
use Drupal\xtc\XtendedContent\API\ToolBox;
use Drupal\xtc\XtendedContent\API\XtcLoaderBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcprofile\Entity\XtcProfile;
use phpDocumentor\Reflection\Types\Mixed_;

/**
 * Plugin implementation of the 'xtcfield_plugin_html_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "xtcfield_plugin_json2yaml_formatter",
 *   label = @Translation("Json to Yaml"),
 *   field_types = {
 *     "xtcfield_plugin_profile",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class XtcFieldPluginJson2Yaml extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => $this->getSetting('theme'),
        '#response' => $this->viewValue($item),
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *
   * @return array
   */
  protected function viewValue(FieldItemInterface $item) {
    $loaded = false;
    $name = $item->getString();
    $content = [];

    try {
      $profile = XtcLoaderProfile::load($name);
      $loaded = true;
    } finally {
      if($loaded) {
        $response = Json::decode(XtcLoaderProfile::content($name));
        $content['body'] = Yaml::encode($response);
        return $content;
      }
      $xtcprofile = XtcProfile::load($name);
      $newprofile = $options = $xtcprofile->get('options');
      $newprofile['label'] = $xtcprofile->label();
      $newprofile['id'] = $xtcprofile->id();

      $handler = XtcLoaderHandler::get($options['handler']);
      $handler->setProfile($newprofile)
        ->setOptions($options);

      $content['body'] = $xtcprofile->get('options');
      return $content;
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'theme' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();
    $registry = theme_get_registry();
    $options = [];
    foreach ($registry as $key => $option) {
      $options[$key] = $key;
    }

    $elements['theme'] = [
      '#type' => 'select',
      '#title' => t('Theme function'),
      '#options' => $options,
      '#description' => t('The theme functions that can be used to display the results.'),
      '#default_value' => $settings['theme'],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $settings = $this->getSettings();

    if (!empty($settings['theme'])) {
      $summary[] = t('Theme function: @theme', ['@theme' => $settings['theme']]);
    }
    else {
      $summary[] = t('No Theme functions is defined.');
    }

    return $summary;
  }

}
