<?php

namespace Drupal\xtcentity\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\xtc\XtendedContent\API\ToolBox;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcprofile\Entity\XtcProfile;

/**
 * Plugin implementation of the 'xtcfield_plugin_html_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "xtcfield_plugin_yaml_formatter",
 *   label = @Translation("YAML structure"),
 *   field_types = {
 *     "xtcfield_plugin_profile",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class XtcFieldPluginYaml extends XtcFieldPluginHtml {

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *
   * @return array
   */
  protected function viewValue(FieldItemInterface $item) {
    $loaded = false;
    $name = $item->getString();
    $content = [];

    try {
      $profile = XtcLoaderProfile::load($name);
      $loaded = true;
    } finally {
      if($loaded) {
        $content['body'] = $this->structToHtml(XtcLoaderProfile::content($name));
        return $content;
      }
      $xtcprofile = XtcProfile::load($name);
      $newprofile = $options = $xtcprofile->get('options');
      $newprofile['label'] = $xtcprofile->label();
      $newprofile['id'] = $xtcprofile->id();

      $handler = XtcLoaderHandler::get($options['handler']);
      $handler->setProfile($newprofile)
        ->setOptions($options);

      $content['body'] = $this->structToHtml($xtcprofile->get('options'));
      return $content;
    }

  }

  /**
   * @param $struct
   *
   * @return mixed
   */
  protected function structToHtml($struct) {
    if(!is_array($struct)) {
      if(!ToolBox::isJson($struct)){
        return $struct;
      }
      $struct = Json::decode($struct);
    }
    $html = '';
    foreach ($struct as $tag => $value) {
      if (is_array($value)) {
        $html .= '<' . $tag . '>';
        $html .= $this->subStructToHtml($tag, $value);
        $html .= "</" . $tag . '>';
      }
      else {
        if (is_int($tag)) {
          $html .= $value;
        }
        else {
          $html .= '<' . $tag . '>' . $value . "</" . $tag . '>';
        }
      }
    }
    return $html;
  }

  protected function subStructToHtml($tag, $struct): string {
    $html = '';
    foreach ($struct as $value) {
      $html .= '<' . $tag . '>' . $value . "</" . $tag . '>';
    }
    return $html;
  }
}
