<?php

namespace Drupal\xtcentity\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcprofile\Entity\XtcProfile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'xtcfield_plugin_label_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "xtcfield_plugin_label_formatter",
 *   label = @Translation("Label"),
 *   field_types = {
 *     "xtcfield_plugin_cache",
 *     "xtcfield_plugin_handler",
 *     "xtcfield_plugin_profile",
 *     "xtcfield_plugin_request",
 *     "xtcfield_plugin_server",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class XtcFieldPluginLabel extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        // Implement default settings.
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
        // Implement settings form.
      ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $loaded = false;
    $name = $item->value;
    try {
      $profile = XtcLoaderProfile::load($name);
      $loaded = true;
    } finally {
      if($loaded) {
        return $profile['label'];
      }
      $profile = XtcProfile::load($name);
      return $profile->label()->__toString();
    }
    //    $options = ToolBox::splitPipe($this->getFieldSetting('xtcoptions_item'));


//    return $item->getString();
  }

}
