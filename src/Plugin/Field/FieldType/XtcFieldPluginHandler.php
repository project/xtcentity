<?php

namespace Drupal\xtcentity\Plugin\Field\FieldType;


/**
 * Plugin implementation of the 'xtcfield_plugin_handler' field type.
 *
 * @FieldType(
 *   id = "xtcfield_plugin_handler",
 *   label = @Translation("XTC Handler plugin"),
 *   description = @Translation("Select an XTC Handler"),
 *   category = @Translation("XTC Plugin Fields"),
 *   default_widget = "xtcfield_options_select",
 *   default_formatter = "xtcfield_label_formatter",
 *   list_class = "\Drupal\Core\Field\FieldItemList",
 *   service = "plugin.manager.xtc_handler",
 * )
 */
class XtcFieldPluginHandler extends XtcFieldPluginBase {

}
