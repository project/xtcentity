<?php

namespace Drupal\xtcentity\Plugin\Field\FieldType;


/**
 * Plugin implementation of the 'xtcfield_plugin_request' field type.
 *
 * @FieldType(
 *   id = "xtcfield_plugin_request",
 *   label = @Translation("XTC Request plugin"),
 *   description = @Translation("Select an XTC Request"),
 *   category = @Translation("XTC Plugin Fields"),
 *   default_widget = "xtcfield_options_select",
 *   default_formatter = "xtcfield_label_formatter",
 *   list_class = "\Drupal\Core\Field\FieldItemList",
 *   service = "plugin.manager.xtc_request",
 * )
 */
class XtcFieldPluginRequest extends XtcFieldPluginBase {

}
