<?php

namespace Drupal\xtcentity\Plugin\Field\FieldType;


/**
 * Plugin implementation of the 'xtcfield_plugin_profile' field type.
 *
 * @FieldType(
 *   id = "xtcfield_plugin_profile",
 *   label = @Translation("XTC Profile plugin"),
 *   description = @Translation("Select an XTC Profile"),
 *   category = @Translation("XTC Plugin Fields"),
 *   default_widget = "xtcfield_options_select",
 *   default_formatter = "xtcfield_plugin_label_formatter",
 *   list_class = "\Drupal\Core\Field\FieldItemList",
 *   service = "plugin.manager.xtc_profile",
 * )
 */
class XtcFieldPluginProfile extends XtcFieldPluginBase {

}
