<?php

namespace Drupal\xtcentity\Plugin\Field\FieldType;


/**
 * Plugin implementation of the 'xtcfield_plugin_cache' field type.
 *
 * @FieldType(
 *   id = "xtcfield_plugin_cache",
 *   label = @Translation("XTC Cache plugin"),
 *   description = @Translation("Select an XTC Cache"),
 *   category = @Translation("XTC Plugin Fields"),
 *   default_widget = "xtcfield_options_select",
 *   default_formatter = "xtcfield_label_formatter",
 *   list_class = "\Drupal\Core\Field\FieldItemList",
 *   service = "plugin.manager.xtc_cache",
 * )
 */
class XtcFieldPluginCache extends XtcFieldPluginBase {

}
