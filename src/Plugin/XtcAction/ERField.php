<?php


namespace Drupal\xtcentity\Plugin\XtcAction;


/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "erfield",
 *   label = @Translation("ER Field for XTC Action"),
 *   description = @Translation("ER Field for XTC Action description.")
 * )
 */

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderAction;

class ERField extends XtcActionPluginBase {

  /**
   * @param array $options
   *
   * @return array
   */
  public function from($options = []) {
    $content = [
      'options' => $options,
      'values' => [],
    ];
    /** @var ContentEntityBase $entity */
    $entity = $options['entity'] ?? NULL;
    $settings = $options['settings'] ?? NULL;

    $fieldname = $settings['field'] ?? '';
    $entityType = $settings['entity_type'] ?? '';

    if(!empty($fieldname) && !empty($entity->get($fieldname))) {
      $targetIds = $entity->get($fieldname)->getValue();
      foreach ($targetIds as $item) {
        $targetId = $item['target_id'];
        /** @var ContentEntityBase $newEntity */
        $newEntity = \Drupal::service('entity_type.manager')
          ->getStorage($entityType)
          ->load($targetId);
        $content['options']['entity'] = $newEntity;

        if(!empty($newEntity->bundle())) {
          $bundle = $settings['bundles'][$newEntity->bundle()] ?? null;
          $content['options']['settings'] = $bundle;
          if(!empty($bundle)) {
            $read = XtcLoaderAction::get($bundle['type']);
            $value = $read->from(['entity' => $newEntity, 'settings' => $bundle]);
            if(!empty($value)) {
              $content['values'][$targetId] = $value;
            }
          }
        }
      }
    }
    return $content;
  }

}
