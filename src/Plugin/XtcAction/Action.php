<?php


namespace Drupal\xtcentity\Plugin\XtcAction;


/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "action",
 *   label = @Translation("Action for XTC Action"),
 *   description = @Translation("Action for XTC Action description.")
 * )
 */
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;

class Action extends XtcActionPluginBase {

}
