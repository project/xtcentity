<?php


namespace Drupal\xtcentity\Plugin\XtcAction;


/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "composite",
 *   label = @Translation("Composite for XTC Action"),
 *   description = @Translation("Composite for XTC Action description.")
 * )
 */

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderAction;

class Composite extends XtcActionPluginBase {

  /**
   * @param array $options
   *
   * @return array
   */
  public function from($options = []) {
    $content = [
      'options' => $options,
      'values' => [],
    ];
    $values = [];
    /** @var ContentEntityBase $entity */
    $entity = $options['entity'] ?? NULL;
    $settings = $options['settings'] ?? NULL;
    foreach ($settings as $name => $setting) {
      $type = key($setting);
      $read = XtcLoaderAction::get($type);
      $items[] = $read->from(['entity' => $entity, 'settings' => $setting[$type]]);
    }
    $text = '';
    foreach ($items as $item) {
      foreach ($item['values'] as $value) {
        if(!empty($value['values'])) {
          foreach ($value['values'] as $val) {
            $text .= $val;
          }
        } else {
          $text .= $value;
        }
      }
    }
    $content['values'] = $text;
    return $content;
  }

}
