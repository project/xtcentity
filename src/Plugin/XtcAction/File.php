<?php


namespace Drupal\xtcentity\Plugin\XtcAction;


/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "file",
 *   label = @Translation("File for XTC Action"),
 *   description = @Translation("File for XTC Action description.")
 * )
 */

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;

class File extends XtcActionPluginBase {

  /**
   * @param array $options
   *
   * @return bool|mixed|void
   */
  public function to($options = []) {
    /** @var ContentEntityBase $entity */
    $entity = $options['entity'] ?? NULL;
    $settings = $options['settings'] ?? [];
    $contents = $options['content'] ?? [];

    if(empty($settings['profile'])) {
      return;
    }
    $this->loadProfile($settings['profile']);
    $handlerOptions = $this->options;
    $contentEntity = $contents['options']['entity'] ?? $entity;
    $contentSettings = $contents['options']['settings'] ?? $settings;

    if(!empty($contents['values']) && !empty($contentEntity)) {
      foreach ($contents['values'] as $id => $content) {
        if(is_array($content)) {
          if(!empty($content['values'][0])) {
            $content = $content['values'][0];
          } elseif(!empty($content[0])) {
            $content = '';
          }
        }
//        $fullname = $this->getFullName($contentEntity, $contentSettings);
        $fullname = $this->getFullName($contentEntity, $settings);
        if(!empty($fullname)) {
          $this->handler->setContent($content);
          $handlerOptions['filename'] = $fullname;
          $this->handler->setProfile($this->profile)
            ->setOptions($handlerOptions);
          $this->handler->process();
        }
      }
    } else {
      $fullname = $this->getFullName($entity, $settings) ?? NULL;
      if(!empty($fullname)) {
        if(!empty($settings['profile'])) {
//          $this->loadProfile($settings['profile']);
          //          $handlerOptions = $this->options;
          $this->handler->setContent('');
          $handlerOptions['filename'] = $fullname;
          $this->handler->setProfile($this->profile)
            ->setOptions($handlerOptions);
          $this->handler->process();
        }
      }
    }
  }
}
