<?php


namespace Drupal\xtcentity\Plugin\XtcAction;


/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "dummy",
 *   label = @Translation("Dummy for XTC Action"),
 *   description = @Translation("Dummy for XTC Action description.")
 * )
 */
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;

class Dummy extends XtcActionPluginBase {

  /**
   * @param array $options
   *
   * @return array
   */
  public function from($options = []) {
    $content = [
      'options' => $options,
      'values' => [],
    ];
    return $content;
  }
}
