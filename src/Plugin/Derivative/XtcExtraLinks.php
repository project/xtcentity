<?php

namespace Drupal\xtcentity\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links for the Products.
 */
class XtcExtraLinks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * Creates a XtcMenuLink instance.
   *
   * @param $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    // We assume we don't have too many...
    $xtctypes = $this->entityTypeManager->getStorage('xtended_content_type')->loadMultiple();
    foreach ($xtctypes as $id => $xtctype) {
      $links[$id] = [
          'title' => $xtctype->label(),
          'parent' => 'entity.xtended_content.add_page',
          'route_name' => 'entity.xtended_content.add',
          'route_parameters' => ['xtended_content_type' => $id]
        ] + $base_plugin_definition;
    }

    return $links;
  }
}
