<?php

namespace Drupal\xtcserver\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xtcserver\Entity\XtcServer;

/**
 * Class XtcServerForm.
 *
 * @package Drupal\xtcserver\Form
 */
class XtcServerForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $xtcserver = $this->entity;

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#description' => $this->t('A human-readable title for this option set.'),
      '#maxlength' => 255,
      '#default_value' => $xtcserver->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $xtcserver->id(),
      '#machine_name' => [
        'exists' => '\Drupal\xtcserver\Entity\XtcServer::load',
      ],
      '#disabled' => !$xtcserver->isNew(),
    ];

    // Options Vertical Tab Group table.
    $form['tabs'] = [
      '#type' => 'horizontal_tabs',
    ];

    $form['definition'] = [
      '#type' => 'details',
      '#title' => $this->t('Definition'),
      '#group' => 'tabs',
      '#open' => FALSE,
    ];
    $form['definition']['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#description' => $this->t('A description for this option set.'),
      '#default_value' => $this->getSetting('description'),
    ];


    $form['base_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Base options'),
      '#group' => 'tabs',
      '#open' => FALSE,
    ];

    $server_types = [
      'guzzle' => 'Guzzle / WebService',
      'graphql' => 'GraphQL',
      'elasticsearch' => 'ElasticSearch',
    ];
    $form['base_options']['type'] = [
      '#title' => $this->t('Type'),
      '#type' => 'select',
      '#description' => $this->t('Select the type of server.'),
      '#options' => $server_types,
      '#default_value' => $this->getSetting('type'),
    ];
    $environments = [
      'dev' => 'Development',
      'int' => 'Integration',
      'test' => 'Test',
      'training' => 'Training',
      'pprod' => 'Pre-production',
      'prod' => 'Production',
    ];
    $form['base_options']['env'] = [
      '#title' => $this->t('Active environment'),
      '#type' => 'select',
      '#options' => $environments,
      '#description' => $this->t('The connection to be used on the current Server.'),
      '#default_value' => $this->getSetting('env'),
    ];

    foreach ($environments as $env => $name) {
      $form['base_options']['enabled_' . $env] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable ' . $name),
        '#default_value' => $this->getSetting('enabled_' . $env),
      ];

      $form[$env . '_connection'] = [
        '#type' => 'details',
        '#title' => $this->t($name),
        '#group' => 'tabs',
        '#open' => FALSE,
      ];
      $form[$env . '_connection']['label_' . $env] = [
        '#title' => $this->t('Label'),
        '#type' => 'textfield',
        '#description' => $this->t('A human-readable title for this option set.'),
        '#maxlength' => 255,
        '#default_value' => $this->getSetting('label_' . $env),
        '#required' => false,
      ];
      $form[$env . '_connection']['id_' . $env] = [
        '#type' => 'machine_name',
        '#default_value' => $this->getSetting('id_' . $env),
        '#machine_name' => [
          'source' => [$env . '_connection','label_' . $env],
          'exists' => [$this, 'exists'],
        ],
        '#required' => false,
      ];
      $form[$env . '_connection']['self_' . $env] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Drupal loop (self)'),
        '#description' => $this->t('Enable this to target the current server.'),
        '#default_value' => $this->getSetting('self_' . $env),
      ];
      $form[$env . '_connection']['tls_' . $env] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable TLS mode'),
        '#description' => $this->t('Enable this if your URL starts with "https://".'),
        '#default_value' => $this->getSetting('tls_' . $env),
      ];
      $form[$env . '_connection']['host_' . $env] = [
        '#type' => 'textfield',
        '#title' => $this->t('Host'),
        '#description' => $this->t('The hostname of the server.'),
        '#default_value' => $this->getSetting('host_' . $env),
      ];
      $form[$env . '_connection']['port_' . $env] = [
        '#type' => 'number',
        '#title' => $this->t('Port'),
        '#description' => $this->t('The port of the server. Usually 443 for HTTPS, or 80 HTTP.'),
        '#default_value' => $this->getSetting('port_' . $env),
      ];
      $form[$env . '_connection']['endpoint_' . $env] = [
        '#type' => 'textfield',
        '#title' => $this->t('Endpoint'),
        '#description' => $this->t('You need to read your service documentation to complete this field.'),
        '#default_value' => $this->getSetting('endpoint_' . $env),
      ];
    }

    return $form;
  }

  protected function getSetting($name) {
    $options = $this->entity->getOptions();
    return isset($options[$name]) ? $options[$name] : XtcServer::getDefault($name);
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\xtcserver\Entity\XtcServer $entity */
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addStatus($this->t('Created the %label XTC Server optionset.', [
            '%label' => $entity->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addStatus($this->t('Saved the %label XTC Server optionset.', [
            '%label' => $entity->label(),
          ]));
    }
    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $options = [];
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      if (in_array($key, ['id', 'label'])) {
        $entity->set($key, $value);
      }
      else {
        $options[$key] = $value;
      }
    }
    $entity->set('options', $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    // Prevent access to delete button when editing default configuration.
    if ($this->entity->id() == 'default' && isset($actions['delete'])) {
      $actions['delete']['#access'] = FALSE;
    }
    return $actions;
  }

  /**
   * Validation functions.
   */
  public function validateNamespace(array &$element, FormStateInterface $form_state) {
    // @todo
    // @see form_error()
    return TRUE;
  }

  /**
   * Validation functions.
   */
  public function validateSelector(array &$element, FormStateInterface $form_state) {
    // @todo
    // @see form_error()
    return TRUE;
  }

  /**
   * Validate thumbnail option values.
   *
   * Empties the value of the thumbnail caption option when the paging control
   * is not set to thumbnails.
   *
   * @param array $element
   *   The element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateThumbnailOptions(array &$element, FormStateInterface $form_state) {
    if ($form_state->getValue('controlNav') !== 'thumbnails' && $element['#value']) {
      $form_state->setValueForElement($element, '');
    }
  }

}
