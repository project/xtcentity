<?php

namespace Drupal\xtcserver\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\xtcentity\Entity\XtcConfigEntityBase;
use Drupal\xtcentity\XtcConfigEntityInterface;
use Drupal\xtcserver\XtcServerDefaults;
use Drupal\xtcserver\XtcServerInterface;

/**
 * Defines the XTC Server entity.
 *
 * @ConfigEntityType(
 *   id = "xtcserver",
 *   label = @Translation("XTC Server"),
 *   handlers = {
 *     "list_builder" = "Drupal\xtcentity\Controller\XtcEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\xtcserver\Form\XtcServerForm",
 *       "edit" = "Drupal\xtcserver\Form\XtcServerForm",
 *       "delete" = "Drupal\xtcentity\Form\XtcEntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "optionset",
 *   admin_permission = "administer xtcserver",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/xtc/xtcserver/{xtcserver}",
 *     "edit-form" = "/admin/config/xtc/xtcserver/{xtcserver}/edit",
 *     "enable" = "/admin/config/xtc/xtcserver/{xtcserver}/enable",
 *     "disable" = "/admin/config/xtc/xtcserver/{xtcserver}/disable",
 *     "delete-form" = "/admin/config/xtc/xtcserver/{xtcserver}/delete",
 *     "collection" = "/admin/config/xtc/xtcserver"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "options",
 *   }
 * )
 */
class XtcServer extends XtcConfigEntityBase implements XtcConfigEntityInterface {

  const DEFAULT = [
    'enabled_dev' => false,
    'self_dev' => false,
    'tls_dev' => false,
    'host_dev' => 'dev.example.com',
    'port_dev' => 80,
    'endpoint_dev' => '',

    'enabled_int' => false,
    'self_int' => false,
    'tls_int' => true,
    'host_int' => 'int.example.com',
    'port_int' => 443,
    'endpoint_int' => '',

    'enabled_test' => false,
    'self_test' => false,
    'tls_test' => true,
    'host_test' => 'test.example.com',
    'port_test' => 443,
    'endpotest_test' => '',

    'enabled_training' => false,
    'self_training' => false,
    'tls_training' => true,
    'host_training' => 'training.example.com',
    'port_training' => 443,
    'endpotraining_training' => '',

    'enabled_pprod' => false,
    'self_pprod' => false,
    'tls_pprod' => true,
    'host_pprod' => 'pprod.example.com',
    'port_pprod' => 443,
    'endpoint_pprod' => '',

    'enabled_prod' => false,
    'self_prod' => false,
    'tls_prod' => true,
    'host_prod' => 'www.example.com',
    'port_prod' => 443,
    'endpoint_prod' => '',

  ];

}
